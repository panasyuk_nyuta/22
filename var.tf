variable "region" {
  type = string
  default = "us-east-1"
  #default = "eu-central-1"
  #default = "us-west-1"
   }


variable "instance_type1" {
  type = string
  default = "t2.nano"
}

variable "network" {
  type = string
  default = "vpc-596aa03e"
 }

variable "subnet" {
  type = string
default = "subnet-3d82d101"
  }

variable "useless" {
  type = string
  default = "<form action=\"http://live.hh.ru\"><input type=\"submit\"></form>"
 }

variable "empty" {
  type = string
}

variable "tags" {
  type = map(any)
  default = {
    test-env-owner = "ann.panasyuk"
  }
}