terraform {
  required_version = "0.14.0"
  backend "remote" {
    hostname = "6799312a3d47.test-env.scalr.com"
    organization = "env-svrcnchebt61e30"
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwianRpIjoiYXQtdGdjZDlvNmFqODlxcWpvIn0.-GtdLworiK2m7ynFziHRGCp81kFL-ZvoztloQJbO0VE"

    workspaces {
      name = "33"
    }
  }
}
provider "aws"  {
  region   = var.region
}
data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}
resource "aws_instance" "ubuntu" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type1
  subnet_id                   = var.subnet
  tags                        = merge({ "Name" = format("ann.panasyuk-test -> %s -> %s", substr("🤔🤷", 0, 1), data.aws_ami.ubuntu.name) }, var.tags)
}